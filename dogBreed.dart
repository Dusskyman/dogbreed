main() {
  Pitbul pit = Pitbul();
  Labrodor labrodor = Labrodor();
  Tax tax = Tax();

  List<IDogBreed> dogs = [pit, labrodor, tax];

  // tellDogBreed(tax);
  // tellDogBreed(labrodor);
  // tellDogBreed(pit);

  tellDogsBreed(dogs);
}

void tellDogBreed(IDogBreed dog) {
  print(dog.breedName);
}

//Util to tell dog breed

void tellDogsBreed(List<IDogBreed> dogs) {
  for (var i = 0; i < dogs.length; i++) {
    print(dogs[i].breedName);
  }
}
//Util to tell all dogs breed

abstract class IDogBreed {
  String breedName;
}

class Pitbul implements IDogBreed {
  String breedName = 'Pitbul';
}

class Labrodor implements IDogBreed {
  String breedName = 'Labrodor';
}

class Tax implements IDogBreed {
  String breedName = 'Tax';
}
